package ru.homeWork.first;

public class Convector {
    private double far,kel,ran,del,cel;

     Convector(double cel) {
        this.far=cel*9/5+32;
        this.kel=cel+273.15d;
        this.ran=(cel+273.15)*9/5;
        this.del=(100-cel)*3/2;
        this.cel=cel;
    }

    public void Show(){
        System.out.println(
                        "Temperature in Celsius is "+ far+"\n"+
                        "Temperature in Fahrenheit is "+kel+"\n"+
                        "Temperature in Rankin is "+ran+"\n"+
                        "Temperature in Delisle is "+del);
    }

}
