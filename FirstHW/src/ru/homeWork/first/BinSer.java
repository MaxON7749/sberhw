package ru.homeWork.first;

import java.util.Arrays;
import java.util.Scanner;

public class BinSer {

    private int k;
    int [] arraySer;

    public BinSer(int k) {
        this.k=k;
        arraySer =new int[k];
        FillArray();

    }


   private void FillArray(){
       Scanner input=new Scanner(System.in);
       System.out.println("Введеите "+k+" чисел");
       for (int i=0;i<k;i++){
           arraySer[i]=input.nextInt();
       }

   }



    public void Search(int [] arraySer, int search){
        int first,last,position;
        Arrays.sort(arraySer);
        first=0;
        last=this.k-1;
        position=(first+last)/2;
        while ((arraySer[position]!=search)&&(first<=last)){
            if(arraySer[position]>search){
                last=position-1;
            }else {
                first=position+1;
            }
            position=(first+last)/2;
        }
        if(arraySer[position]==search){
            System.out.println(search+" явдяется "+ ++position+ " элементом в массиве");
        }else System.out.println("Error");
    }



}
