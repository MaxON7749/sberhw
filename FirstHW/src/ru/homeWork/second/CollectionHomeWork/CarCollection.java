package ru.homeWork.second.CollectionHomeWork;

public class CarCollection implements Comparable<CarCollection> {
   private String model;
    private String type;


    public CarCollection(String model, String type) {
        this.model = model;
        this.type = type;
    }


    public String getModel() { return model; }

    public void setModel(String model) { this.model = model; }

    public String getType() { return type; }

    public void setType(String type) { this.type = type; }

    @Override
    public int compareTo(CarCollection o) {
        return  getType().compareTo(o.getType());
    }

    @Override
    public String toString(){
        return "{Model: "+getModel()+" type: "+getType();
    }
}
