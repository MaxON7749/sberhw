package ru.homeWork.second.CollectionHomeWork;

import java.util.*;

public class MainCollection {
    public static void main(String[] args) {
        ArrayList<CarCollection> carCollections=new ArrayList<>();
        carCollections.add(new CarCollection("Лада","седан"));
        carCollections.add(new CarCollection("Мерседес","седан"));
        carCollections.add(new CarCollection("Бмв","кроссовер"));
        carCollections.add(new CarCollection("Форд","хэтчбек"));
        carCollections.add(new CarCollection("Пежо","кроссовер"));
        carCollections.add(new CarCollection("Тойота","седан"));
        carCollections.add(new CarCollection("Тойота","седан"));
        Collections.sort(carCollections);
        carCollections.forEach(System.out::println);
    }

}
