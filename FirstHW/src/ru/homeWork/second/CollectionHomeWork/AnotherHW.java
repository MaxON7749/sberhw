package ru.homeWork.second.CollectionHomeWork;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Scanner;
import java.util.function.Consumer;

public class AnotherHW {
    public static void main(String[] args) throws FileNotFoundException {
        Comparator<String> pcmp=new FirstComporator().thenComparing(new SecondComporator());
        ArrayList<String> arrayList=new ArrayList<>();
        arrayList=ReadText();
        System.out.println(arrayList.size());
        arrayList.forEach(o-> System.out.println(o));
        Collections.sort(arrayList, pcmp);
        System.out.println("----------------------Sorted----------------------");
        arrayList.forEach(o-> System.out.println(o));

        System.out.println("--------------------------------------------");
        int [] arrayCount=new int[5];
        arrayCount[0]=1;
        arrayCount[1]=2;
        arrayCount[3]=5;
        arrayCount[4]=8;
        arrayCount[2]=8;
        for(int i=0;i<arrayCount.length;i++)
        System.out.println(arrayList.get(arrayCount[i]));
    }


    public static ArrayList<String> ReadText(){
        String path ="C://Users/negod/Desktop/text.txt";
        File file=new File(path);
        ArrayList<String> arrayList=new ArrayList<>();
        try {
            Scanner scanner = new Scanner(file);
            //System.out.println("Чтение есть");
            while (scanner.hasNextLine()) {
                arrayList.add(scanner.nextLine());
            }
        }catch (FileNotFoundException e){

        }
        return arrayList;
    }
}

class FirstComporator implements Comparator<String>{

    @Override
    public int compare(String o1, String o2) {
        return o1.length()-o2.length();
    }
}

class SecondComporator implements Comparator<String>{

    @Override
    public int compare(String o1, String o2) {
        return o1.toString().compareTo(o2.toString());
    }
}
