package ru.homeWork.second;

import java.util.ArrayList;
import java.util.Collection;

public class Exm1_ArrayList {
    public static void main(String[] args) {
        ArrayList<PersoneTest> personeTests = new ArrayList<>();
        ArrayList<PersoneTest> personeTests2 = new ArrayList<>();
        PersoneTest o1 = new PersoneTest(16L, "Тест Тестович Тестов", "+7938101109");
        PersoneTest o2 = new PersoneTest(17L, "Тест1 Тестович Тестов", "+7938101118");
        PersoneFirstLoad.init(personeTests);
        PersoneFirstLoad.init(personeTests2);
        PersoneFirstLoad.print(personeTests);
//        System.out.println(personeTests.size());
//        System.out.println("---------------------------------add--------------------------------");
//        personeTests.add(5,o1);
//        personeTests.getClass();
//        personeTests.forEach(System.out::println);
//        System.out.println(personeTests.getClass());
//
//        System.out.println("---------------------------------remove--------------------------------");
//        personeTests.remove(o2);
//        personeTests.forEach(System.out::println);
//        System.out.println(personeTests.size());
//        System.out.println("---------------------------------addAll--------------------------------");
//        personeTests.addAll(personeTests2);
//        personeTests.forEach(System.out::println);
//        System.out.println(personeTests.indexOf(o1));

    }
}
