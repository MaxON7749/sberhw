package ru.homeWork.second;

import java.util.LinkedList;
import java.util.function.Consumer;

public class Exm2_LinkedList {
    public static void main(String[] args) {
        //Consumer<Object> print = (o) ->{ System.out.println("object->"); System.out.println(o);};
        LinkedList<PersoneTest> personeTests=new LinkedList<>();
        PersoneTest o1 = new PersoneTest(11L,"Тест Тестович Тестов", "+7938101109");
        PersoneTest o2 = new PersoneTest(17L,"Тест1 Тестович Тестов", "+7938101118");
        PersoneFirstLoad.init(personeTests);
        PersoneFirstLoad.print(personeTests);
        personeTests.add(1, o1);
        personeTests.remove(1);
        personeTests.addLast(o2);

        System.out.println("_________________BeforePeek_____________________");
        System.out.println(personeTests.peek());
        System.out.println("_________________AfterPeek_____________________");
        personeTests.forEach(System.out::println);

        System.out.println("_________________BeforPool_____________________");
        System.out.println(personeTests.poll());
        System.out.println("_________________AfterPool_____________________");
        personeTests.forEach(System.out::println);


        System.out.println("_________________poolLast_____________________");

        personeTests.pollLast();

        personeTests.forEach(System.out::println);
       // personeTests.forEach(print);
    }
}
