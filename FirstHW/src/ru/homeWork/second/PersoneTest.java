package ru.homeWork.second;

public class PersoneTest implements Comparable <PersoneTest> {
    Long id;
    String name;
    String phoneNum;

    public PersoneTest(Long id, String name, String phoneNum) {
        this.id = id;
        this.name = name;
        this.phoneNum = phoneNum;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) { this.id = id; }

    public String getName() { return name; }

    public void setName(String name) { this.name = name; }

    public String getPhoneNum() { return phoneNum; }

    public void setPhoneNum(String phoneNum) { this.phoneNum = phoneNum; }

    @Override
    public String toString(){
        return "{Persone="+this.getId()+"; name= "+this.getName()+"; phoneNumber="+this.getPhoneNum()+"}";
    }

    @Override
    public boolean equals (Object obj) { return this.getId().equals(((PersoneTest)obj).getId());}

    @Override
    public int compareTo(PersoneTest o) {
        return this.getPhoneNum().compareTo((o.getPhoneNum()));
    }

    @Override
    public int hashCode(){
        return (this.getId()+this.getName()+this.getPhoneNum()).hashCode();
    }
}
