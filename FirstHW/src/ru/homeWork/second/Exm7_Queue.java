package ru.homeWork.second;

import java.util.LinkedList;
import java.util.Locale;
import java.util.Queue;
import java.util.function.Consumer;

public class Exm7_Queue {

    public static void main(String[] args) {
        Queue<PersoneTest> personeTests=new LinkedList<>();
        PersoneFirstLoad.init(personeTests);
        PersoneFirstLoad.print(personeTests);
        PersoneTest o = new PersoneTest(54L,"Тест Тестович Тестов", "+7938101109");
        System.out.println("_________________AfterPeek_____________________");
        System.out.println(personeTests.peek());
        System.out.println("_________________AfterPeek_____________________");
        System.out.println(personeTests.poll());
        System.out.println("_________________BeforPool_____________________");
        System.out.println(personeTests.offer(o));
        personeTests.forEach(personeTest-> System.out.println(personeTest));

//        Consumer<String> printUpperCase = str -> System.out.println(str.toUpperCase());
//        printUpperCase.accept("hello");
//
//        Consumer<String> printLowerCase = str -> System.out.println(str.toLowerCase());
//        printUpperCase.andThen(printLowerCase).accept("Hello world");

    }
}
