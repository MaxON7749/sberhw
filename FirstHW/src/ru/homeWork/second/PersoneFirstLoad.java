package ru.homeWork.second;

import java.util.Collection;
import java.util.Iterator;
import java.util.Map;

public class PersoneFirstLoad {

    public static Collection<PersoneTest> init (Collection<PersoneTest> persons){
       // PersoneTest personeTest = new PersoneTest(11L,"Тест Тестович Тестов", "+7938101109");
        persons.add(new PersoneTest(1L,"Первый","+7938101100"));
        persons.add(new PersoneTest(2L,"Второй","+7938101101"));
        persons.add(new PersoneTest(3L,"Третий","+7938101102"));
        persons.add(new PersoneTest(4L,"Четвертый","+7938101103"));
        persons.add(new PersoneTest(5L,"Пятый","+7938101104"));
        persons.add(new PersoneTest(6L,"Шестой","+7938101105"));
        persons.add(new PersoneTest(7L,"Седьмой","+7938101106"));
        persons.add(new PersoneTest(8L,"Восьмой","+7938101107"));
        persons.add(new PersoneTest(9L,"Девятый","+7938101108"));
        persons.add(new PersoneTest(10L,"Десятый","+7938101109"));
        //persons.add(personeTest);
        persons.add(new PersoneTest(11L,"Одиннадцатый","+7938101110"));
        persons.add(new PersoneTest(12L,"Двенадцатый","+7938101111"));
        persons.add(new PersoneTest(13L,"Тринадцатый","+7938101112"));
        persons.add(new PersoneTest(14L,"Четырнадцатый","+7938101113"));
        persons.add(new PersoneTest(15L,"Пятнадцыатый","+7938101115"));
        persons.add(new PersoneTest(16L,"Пятнадцыатый","+7938101115"));
        persons.add(new PersoneTest(17L,"Шестнадцатый","+7938101114"));
        persons.add(new PersoneTest(18L,"Шестнадцатый","+7938101188"));
        return persons;
    }

    public static Map<String,PersoneTest> init (Map<String,PersoneTest> persons){
        PersoneTest personeTest = new PersoneTest(11L,"Тест Тестович Тестов", "+7938101109");
        persons.put("1", new PersoneTest(1L,"Первый","+7938101100"));
        persons.put("2", new PersoneTest(2L,"Второй","+7938101101"));
        persons.put("3", new PersoneTest(3L,"Третий","+7938101102"));
        persons.put("4",new PersoneTest(4L,"Четвертый","+7938101103"));
        persons.put("5",new PersoneTest(5L,"Пятый","+7938101104"));
        persons.put("6",new PersoneTest(6L,"Шестой","+7938101105"));
        persons.put("7",new PersoneTest(7L,"Седьмой","+7938101106"));
        persons.put("8",new PersoneTest(8L,"Восьмой","+7938101107"));
        persons.put("9",new PersoneTest(9L,"Девятый","+7938101108"));
        persons.put("10",new PersoneTest(10L,"Десятый","+7938101109"));
        persons.put("11",personeTest);
        persons.put("11",new PersoneTest(11L,"Одиннадцатый","+7938101110"));
        persons.put("12",new PersoneTest(12L,"Двенадцатый","+7938101111"));
        persons.put("13",new PersoneTest(13L,"Тринадцатый","+7938101112"));
        persons.put("14",new PersoneTest(14L,"Четырнадцатый","+7938101113"));
        persons.put("15",new PersoneTest(15L,"Пятнадцыатый","+7938101114"));
        persons.put("15",new PersoneTest(15L,"Пятнадцыатый","+7938101114"));
        persons.put("16",new PersoneTest(16L,"Шестнадцатый","+7938101115"));
        return persons;
    }

    public static void print(Collection<PersoneTest> persons){
        Iterator<PersoneTest> it= persons.iterator();
        while (it.hasNext()){
            System.out.println(it.next());
        }
    }
    public static void print (Map<String,PersoneTest> persons){
        Iterator it= persons.entrySet().iterator();
        while (it.hasNext()){
            System.out.println(it.next().toString());
        }
    }
}
