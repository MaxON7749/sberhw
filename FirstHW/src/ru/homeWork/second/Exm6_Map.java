package ru.homeWork.second;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class Exm6_Map {
    public static void main(String[] args) {
        Map<String,PersoneTest> personeTestMap=new HashMap<>();
        PersoneFirstLoad.init(personeTestMap);
        PersoneFirstLoad.print(personeTestMap);
        System.out.println("size="+personeTestMap.size());
        System.out.println(personeTestMap.get("15"));
        System.out.println("----------------------------------------------------");
        Iterator<PersoneTest> it =personeTestMap.values().iterator();
        it.forEachRemaining(personeTest -> {System.out.println(personeTest);});
        System.out.println("----------------------------------------------------");
        Iterator<String> kit=personeTestMap.keySet().iterator();
        kit.forEachRemaining(key->{ System.out.println(key);});
    }
}
