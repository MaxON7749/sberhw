package ru.homeWork.second;

import java.util.HashSet;
import java.util.Set;

public class Exm5_HashSet {

    public static void main(String[] args) {
        Set<PersoneTest> personeTests=new HashSet<>();
        PersoneFirstLoad.init(personeTests);
        PersoneFirstLoad.print(personeTests);
        System.out.println(personeTests.size());
    }
}
