package ru.homeWork.SerializationStud;

import java.io.Serializable;

public class SerializationClass implements Serializable {
    int a;
    String str;

    public int getA() {
        return a;
    }

    public void setA(int a) {
        this.a = a;
    }

    public String getStr() {
        return str;
    }

    public void setStr(String str) {
        this.str = str;
    }
}
